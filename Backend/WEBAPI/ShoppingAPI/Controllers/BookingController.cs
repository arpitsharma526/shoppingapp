﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShoppingAPI.Model;
using ShoppingAPI.Service;

namespace ShoppingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        readonly IBookingService _bookingService;
        public BookingController(IBookingService bookingService)
        {
            _bookingService=bookingService;
        }
        [Route("AddToCart")]
        [HttpPost]
        public ActionResult AddtoCart(Booking booking)
        {
            bool AddToCartStatus = _bookingService.AddToCart(booking);
            return Ok(AddToCartStatus);
        }
        [Route("GetCartByUsername")]
        [HttpGet]
        public ActionResult GetCartByUsername(string username)
        {
            List<Booking> booking = _bookingService.GetCartByUsername(username);
            return Ok(booking);
        }
        [Route("DeleteCart")]
        [HttpDelete]
        public ActionResult Delete( DeleteCart deleteCart)
        {
            bool DeleteStatus = _bookingService.Delete(deleteCart);
            return Ok(DeleteStatus);

            
        }


    }
}
